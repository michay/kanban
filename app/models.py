from django.contrib.auth.models import User
from django.db import models


class Task(models.Model):
    author = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=True, null=False)
    title = models.CharField(max_length=254, blank=False, null=False)
    content = models.TextField(blank=True, null=True)
